package com.bootcamp;

import org.junit.jupiter.api.Test;

import static com.bootcamp.Category.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class YachtTest {
    @Test
    void expectToGiveScore50ForYacht() {
        assertEquals(50, new Yacht("11111", YACHT).score());
    }

    @Test
    void expectToGiveZeroForYacht() {
        assertEquals(0, new Yacht("16540", YACHT).score());
    }

    @Test
    void expectSumForCategoryChoice() {
        assertEquals(18, new Yacht("23346", CHOICE).score());
        assertEquals(20, new Yacht("44444", CHOICE).score());
    }

    @Test
    void expectScoreToBEThirtyForBigStraight() {
        assertEquals(30, new Yacht("23456", BIG_STRAIGHT).score());
    }

    @Test
    void expectScoreToBEZeroForNoBigStraight() {
        assertEquals(0, new Yacht("666666", BIG_STRAIGHT).score());
    }

    @Test
    void expectScoreToBeThirtyForLittleStraight() {
        assertEquals(30, new Yacht("12345", LITTLE_STRAIGHT).score());
    }

    @Test
    void expectScoreForFourOfAKind() {
        assertEquals(12, new Yacht("33334", FOUR_OF_A_KIND).score());
    }

    @Test
    void expectScoreZeroNotForFourOfAKind() {
        assertEquals(0, new Yacht("33333", FOUR_OF_A_KIND).score());
    }

    @Test
    void expectSumForFullHouse() {
        assertEquals(19, new Yacht("55225", FULL_HOUSE).score());
    }

    @Test
    void expectScoreToBeZeroForNoFullHouse() {
        assertEquals(0, new Yacht("11223", FULL_HOUSE).score());
    }

    @Test
    void expectScoreToBeMultipleOfSixes() {
        assertEquals(36, new Yacht("666666", SIXES).score());
    }

    @Test
    void expectScoreToBeProductOfFiveAndItsCount() {
        assertEquals(25, new Yacht("552555", FIVE).score());
    }

    @Test
    void expectScoreToBeProductOfFourAndItsCount() {
        assertEquals(16, new Yacht("442446", FOUR).score());
    }

    @Test
    void expectScoreToBEProductOfThreeAndItsCount() {
        assertEquals(9, new Yacht("132343", THREE).score());
    }

    @Test
    void expectScoreToBeProductOfTwoAndItsCount() {
        assertEquals(4, new Yacht("123426", TWO).score());
    }

    @Test
    void expectScoreToBeProductOfOneAndItsCount() {
        assertEquals(1, new Yacht("234165", ONE).score());
    }
}
