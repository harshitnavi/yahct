package com.bootcamp;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public enum Category {
    YACHT("XXXXX") {
        public static final int INDEX_ZERO = 0;
        int score(String inputString) {
            if (isYacht(inputString))
                return 50;
            return 0;
        }

        private boolean isYacht(String inputString) {
            for (char digit : inputString.toCharArray()) {
                if (digit != inputString.charAt(INDEX_ZERO))
                    return false;
            }
            return true;
        }
    },
    CHOICE("ABCDE") {
        int score(String inputString) {
            return sum(inputString);
        }
    },
    BIG_STRAIGHT("23456") {
        int score(String inputString) {
            if (inputString.equals("23456")) {
                return 30;
            }
            return 0;
        }
    },
    LITTLE_STRAIGHT("12345") {
        int score(String inputString) {
            if (inputString.equals("12345")) {
                return 30;
            }
            return 0;
        }
    },
    FOUR_OF_A_KIND("XXXXY") {
        int score(String inputString) {
            return sumOfFour(inputString);
        }

        private int sumOfFour(String inputString) {
            HashMap<Character, Integer> frequency = new HashMap<>();
            for (char digit : inputString.toCharArray()) {
                frequency.compute(digit, (key, value) -> value == null ? 1 : value + 1);
            }
            return frequency.entrySet().stream().filter(me ->
                    me.getValue() == FREQUENCY).findFirst().map(me -> me.getKey() * FREQUENCY).orElse(0);
        }
    },
    FULL_HOUSE() {
        int score(String inputString) {
            if (isFullHouse(inputString)) {
                return sum(inputString);
            }
            return 0;
        }

        private boolean isFullHouse(String inputString) {
            Map<Character, Integer> frequency = new TreeMap<>();
            for (char digit : inputString.toCharArray()) {
                frequency.compute(digit, (key, value) -> value == null ? 1 : value + 1);
            }
            if (frequency.size() != 2) {
                return false;
            }
            Iterator<Character> iterator = frequency.keySet().iterator();
            return frequency.get(iterator.next()) == FREQUENCY_OF_TWO || frequency.get(iterator.next()) == FREQUENCY_OF_THREE;
        }
    },
    SIXES("6 * frequency of 6"),
    FIVE("5 * frequency of 5"),
    FOUR("4 * frequency of 4"),
    THREE("3 * frequency of 3"),
    TWO("2 * frequency of 2"),
    ONE("1 * frequency of 1");

    public static final int FREQUENCY_OF_THREE = 3;
    public static final int FREQUENCY_OF_TWO = 2;
    public static final int FREQUENCY = 4;
    public static final int indexZero = 0;
    public static final int baseAscii = 48;
    private String digit;

    Category(String digit) {
        this.digit = digit;
    }

    Category() {
    }

    int score(String inputString) {
        int countDigit = 0;
        for (char digits : inputString.toCharArray()) {
            if (digits == digit.charAt(indexZero)) {
                countDigit++;
            }
        }
        return (digit.charAt(indexZero) - baseAscii) * countDigit;
    }

    private static int sum(String inputString) {
        if (inputString.length() == 1)
            return Integer.parseInt(inputString);
        return inputString.charAt(indexZero) - baseAscii + sum(inputString.substring(1));
    }

}
