package com.bootcamp;

class Yacht {
    private String inputString;
    private Category category;

    Yacht(String inputString, Category category) {
        this.inputString = inputString;
        this.category = category;
    }

    int score() {
        return category.score(inputString);
    }
}
