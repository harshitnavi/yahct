FROM gocd/gocd-agent-ubuntu-16.04:v17.9.0

RUN apt update && apt install -y default-jre && apt install -y default-jdk && apt install wget && wget https://services.gradle.org/distributions/gradle-5.0-bin.zip -P /tmp
RUN unzip -d /opt/gradle /tmp/gradle-*.zip
ENV GRADLE_HOME=/opt/gradle/gradle-5.0
ENV PATH=${GRADLE_HOME}/bin:${PATH}